package com.example.practica2c1java;

//Librerias necesarias
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
public class MainActivity extends AppCompatActivity{
    //Declaración de variables
    private Button btnCalcular, btnLimpiar, btnRegresar;
    private TextView lblTotal;
    private EditText txtPeso, txtAltura;

    //Funcion de la actividad
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        //Se obtienen los elementos del layaout del activity_main
        setContentView(R.layout.activity_main);

        //Se crear la relación entre las variables y los elementos graficos
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        lblTotal = (TextView) findViewById(R.id.lblTotal);
        txtAltura = (EditText) findViewById(R.id.txtAltura);
        txtPeso = (EditText) findViewById(R.id.txtPeso);

        //Codificación de la función calcular
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtPeso.getText().toString().matches("") && txtAltura.getText().toString().matches("")) {
                    //No se capturaron la información
                    Toast.makeText(MainActivity.this, "FAVOR DE INGRESAR LOS DATOS",
                            Toast.LENGTH_SHORT).show();
                } else {
                    float peso = 0.0F, altura = 0.0F, total = 0.0F;
                    peso = Float.parseFloat(txtPeso.getText().toString());  //Conversion de cadena a flotante.
                    altura = Float.parseFloat(txtAltura.getText().toString());
                    total = peso / (altura * altura);
                    lblTotal.setText(Float.toString(total));
                }
            }
        });

        //Codificacion del boton limpiar
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lblTotal.setText("");
                txtAltura.setText("");
                txtPeso.setText("");
                txtAltura.requestFocus();
                txtPeso.requestFocus();
            }
        });

        //Codificación del boton regresar
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
